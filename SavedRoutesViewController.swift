//
//  SavedRoutesViewController.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/04/10.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit

class SavedRoutesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var editButton: UIBarButtonItem!
	@IBOutlet weak var cancelButton: UIBarButtonItem!
	@IBOutlet weak var deleteButton: UIBarButtonItem!
	
	
	var sections : [NSDate : [Route]] = [:]
	var sortedDictionaryKeys : [NSDate] = []
	
    override func viewDidLoad() {
        super.viewDidLoad()
		global.dateFormatter.locale = NSLocale.currentLocale()
    }
	
	override func viewWillAppear(animated: Bool) {
		populateDataSource()
		self.navigationController?.navigationBarHidden = false
		self.navigationItem.rightBarButtonItem = editButton
	}
	
	// ******** Actions
	
	@IBAction func editPressed(sender : AnyObject) {
		self.navigationItem.setRightBarButtonItem(cancelButton, animated: true)
		self.navigationItem.setLeftBarButtonItem(deleteButton, animated: true)
		tableView.setEditing(true, animated: true)
	}
	
	@IBAction func cancelPressed(sender : AnyObject) {
		self.navigationItem.setRightBarButtonItem(editButton, animated: true)
		self.navigationItem.leftBarButtonItems?.removeLast()
		tableView.setEditing(false, animated: true)
	}
	
	@IBAction func deletePressed(sender : AnyObject) {
		let alertController = UIAlertController(title: "Are you sure?", message: "Confirm you would like to delete the selected items", preferredStyle: .Alert)
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
			println(action)
		}
		alertController.addAction(cancelAction)
		
		let destroyAction = UIAlertAction(title: "Delete", style: .Destructive) { (action) in
			
			let selectedRows = self.tableView.indexPathsForSelectedRows()
			
			if selectedRows != nil {
				var indexsOfItemsToDelete : NSMutableIndexSet!
				for indexPath in selectedRows as! [NSIndexPath] {
					let dictElement = self.sections[self.sortedDictionaryKeys[indexPath.section]]
					let route = dictElement![indexPath.row]
					
					routesHelper.deleteSavedRoute(route)
					self.populateDataSource()
				}
				if self.sortedDictionaryKeys.count == 0 {
					self.tableView.reloadData()
				} else {
					self.tableView.deleteRowsAtIndexPaths(selectedRows!, withRowAnimation: .Automatic)
				}
			} else {
				routesHelper.savedRoutes.removeAll(keepCapacity: false)
				self.populateDataSource()
				self.tableView.reloadData()
			}
			self.cancelPressed(self.cancelButton)
		}
		alertController.addAction(destroyAction)
		
		self.presentViewController(alertController, animated: true) {
			// ...
		}
	}
	
	// ******** Functions
	
	func populateDataSource() {
		sections = [:]
		sortedDictionaryKeys = []
		
		for (id, route) in routesHelper.savedRoutes {
			let comps = NSCalendar.currentCalendar().components(.CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay, fromDate: route.date)
			let tempDate = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)?.dateFromComponents(comps)
			if sections[tempDate!] != nil {
				sections[tempDate!]?.append(route)
			} else {
				sections[tempDate!] = [route]
			}
		}
		
		// Sort dictionary - by creating a new array of keys, sorting them and using them to call from the dictionary
		for (date, routes) in sections {
			sortedDictionaryKeys.append(date)
		}
		sortedDictionaryKeys.sort({ $0.compare($1) == NSComparisonResult.OrderedDescending })
	}
	
	func updateDeleteTitle() {
		
		let selectedRows = tableView.indexPathsForSelectedRows()
		
		if selectedRows != nil {
			let allItemsAreSelected = selectedRows!.count == routesHelper.savedRoutes.count
			
			if allItemsAreSelected {
				deleteButton.title = "Delete All"
			} else {
				deleteButton.title = "Delete (\(selectedRows!.count))"
			}
		} else {
			deleteButton.title = "Delete All"
		}
		self.navigationItem.setLeftBarButtonItem(deleteButton, animated: true)
	}
	
	
	//
	//
	// ******** TableView delegate methods
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return sortedDictionaryKeys.count
	}
	
	func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		global.dateFormatter.dateFormat = "dd MMMM"
		let rawDate = sortedDictionaryKeys[section]
		return global.dateFormatter.stringFromDate(rawDate)
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return sections[sortedDictionaryKeys[section]]!.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		global.dateFormatter.dateFormat = "MMM dd, yyyy, HH:mm"
		let dictElement = sections[sortedDictionaryKeys[indexPath.section]]
		let route = dictElement![indexPath.row]
		var cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! SavedRoutesTableViewCell
		cell.lblName.text = route.name
		cell.from = route.from
		cell.to = route.to
		cell.build()
		return cell
	}
	
	func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
		
		let dictElement = self.sections[self.sortedDictionaryKeys[indexPath.section]]
		let route = dictElement![indexPath.row]
		
		var deleteRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Delete", handler:{action, indexpath in
			routesHelper.deleteSavedRoute(route)
			self.populateDataSource()
			self.tableView.reloadData()
		});
	
		return [deleteRowAction]
	}
	
	func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) { }
	
	func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) { }
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if tableView.editing == false {
			let dictElement = self.sections[self.sortedDictionaryKeys[indexPath.section]]
			let route = dictElement![indexPath.row]
			let vc = self.storyboard?.instantiateViewControllerWithIdentifier("captureViewController") as! CaptureViewController
			vc.routeObject = route
			self.navigationController?.pushViewController(vc, animated: true)
		} else {
			updateDeleteTitle()
		}
	}
	
	func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
		updateDeleteTitle()
	}


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
