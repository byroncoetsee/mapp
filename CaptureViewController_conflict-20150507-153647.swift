//
//  CaptureViewController1.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/05/05.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit
import CoreLocation

class CaptureViewController: UIViewController, CLLocationManagerDelegate, UIGestureRecognizerDelegate {
	
	@IBOutlet weak var containerView: UIView!
	@IBOutlet var btnImage: UIButton!
	@IBOutlet weak var btnStart: UIButton!
	
	// Variables
	
	var child : CaptureTableViewController!
	var routeObject : Route!
	var manager : CLLocationManager = CLLocationManager()
	var timerRecordCoords : NSTimer?
	var timerDuration : NSTimer?
	var routeCoords : [CLLocation] = []
	var currentCoords : CLLocation?
	var tracking : Bool = false
	
	var stopCount = 0
	var duration = 0
	var distance = 0.0
	
	var finishTimer : NSTimer!
	var longPress : UILongPressGestureRecognizer!
	var circle : CircleView!

    override func viewDidLoad() {
        super.viewDidLoad()
		
		setRightBarButtonItem()
		
		longPress = UILongPressGestureRecognizer(target: self, action: "startLongPress:")
		longPress.delegate = self
		longPress.minimumPressDuration = 3
		longPress.allowableMovement = 500
		btnStart.addGestureRecognizer(longPress)
		
		let shortPress = UITapGestureRecognizer(target: self, action: "startShortPress:")
		shortPress.delegate = self
		btnStart.addGestureRecognizer(shortPress)
		
		child = self.childViewControllers[0] as! CaptureTableViewController
		
		manager.requestAlwaysAuthorization()
		manager.delegate = self
		manager.distanceFilter = 10
		manager.desiredAccuracy = kCLLocationAccuracyBest
		
		self.navigationController?.navigationBar.barTintColor = UIColor(red:0.33, green:0.32, blue:0.32, alpha:1)
		self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"Back", style:.Plain, target:nil, action:nil)
		
			btnStart.layer.cornerRadius = 0.5 * btnStart.bounds.size.width
			btnStart.backgroundColor = UIColor(red:0.43, green:0.42, blue:0.42, alpha:1)
		
		for coord in routeObject.coordsList {
			routeCoords.append(CLLocation(latitude: coord.latitude, longitude: coord.longitude))
//			btnStart.setTitle("RESTART", forState: UIControlState.Normal)
		}
		
		stopCount = routeObject.stopCount
		duration = routeObject.duration
		distance = routeObject.distance
		
//		child.lblTransportCompany.text = routeObject.company
//		child.lblRouteName.text = routeObject.name
		child.lblFrom.text = routeObject.from
		child.lblTo.text = routeObject.to
		child.lblDuration.text = "\(duration)"
		child.lblDistance.text = "\(distance)"
		
		circle = CircleView(frame: CGRectMake(btnStart.frame.origin.x - 10, btnStart.frame.origin.y - 10, btnStart.frame.size.width + 20, btnStart.frame.size.height + 20))
//		btnStart.insertSubview(circle, atIndex: 999)
		self.view.insertSubview(circle, belowSubview: btnStart)
    }
	
	@IBAction func touchDown(sender: AnyObject) {
		println("TOUCH DOWN")
		if tracking == true {
			btnStart.setTitle("Hold to finish", forState: UIControlState.Normal)
			circle.animateCircle(3)
		}
	}
	
	func startShortPress(sender: UITapGestureRecognizer) {
		println("SHORT TOUCH DOWN")
		circle.resetCircle()
		if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.AuthorizedAlways {
			if tracking == false {
				if routeCoords.isEmpty {
					start()
				} else {
					var saveAlert = UIAlertController(title: "Are you sure?", message: "This will clear your progress and restart the route", preferredStyle: UIAlertControllerStyle.Alert)
					saveAlert.addAction(UIAlertAction(title: "Yes", style: .Destructive, handler: { (action: UIAlertAction!) in
						self.start()
					}))
					saveAlert.addAction(UIAlertAction(title: "No", style: .Default, handler: { (action: UIAlertAction!) in }))
					presentViewController(saveAlert, animated: true, completion: nil)
				}
			} else {
				incrementStopCount()
			}
		}
	}
	
	func startLongPress(sender: UILongPressGestureRecognizer) {
		println("LONG TOUCH DOWN")
		circle.resetCircle()
		longPress.enabled = false
		longPress.enabled = true
		if tracking == true {
			btnStart.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
			end()
		}
	}
	
	@IBAction func touchUp(sender: AnyObject) {
		println("TOUCH UP")
		btnStart.setTitle("\(stopCount)", forState: UIControlState.Normal)
		circle.resetCircle()
	}
	
	func incrementDuration() {
		++duration
		if duration < 60 {
			child.lblDuration.text = "\(duration) s"
		} else {
			child.lblDuration.text = "\(Int(duration/60)) m"
		}
	}
	
	func incrementStopCount() {
		++stopCount
		routeObject.addStopCoords(manager.location)
		btnStart.setTitle("\(stopCount)", forState: UIControlState.Normal)
	}
	
	func saveCoords() {
		if currentCoords != nil {
			routeObject.addCoords(currentCoords!)
		}
	}
	
	func start() {
		tracking = true
		self.navigationController?.navigationBar.barTintColor = UIColor(red:0.89, green:0, blue:0.24, alpha:1)
		self.navigationItem.hidesBackButton = true
		setLeftBarButtonItem()
		setRightBarButtonItem()
		btnStart.setTitle("Tap to add stop", forState: UIControlState.Normal)
		self.routeCoords = []
		self.stopCount = 0
		self.duration = 0
		self.distance = 0
		timerRecordCoords = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: "saveCoords", userInfo: nil, repeats: true)
		timerDuration = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "incrementDuration", userInfo: nil, repeats: true)
		child.lblDuration.text = "\(duration)"
		child.lblDistance.text = "\(distance) km"
		manager.startUpdatingLocation()
		child.lblGpsAccuracy.text = "9999m"
	}
	
	func end() {
		tracking = false
		self.navigationController?.navigationBar.barTintColor = UIColor(red:0.33, green:0.32, blue:0.32, alpha:1)
		self.navigationItem.leftBarButtonItems = nil
		self.navigationItem.hidesBackButton = false
//		btnStart.setTitle("RESTART", forState: UIControlState.Normal)
		timerRecordCoords?.invalidate()
		timerRecordCoords = nil
		timerDuration?.invalidate()
		timerDuration = nil
		currentCoords = nil
		child.lblGpsAccuracy.text = "OFF"
		if routeObject.stopCoordsList.isEmpty == false && currentCoords != nil {
			routeObject.addCoords(currentCoords!)
			incrementStopCount()
		}
		manager.stopUpdatingLocation()
		println("END")
	}
	
	func setRightBarButtonItem() {
		let buttonEdit: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
		buttonEdit.frame = CGRectMake(0, 0, 40, 40)
		buttonEdit.setImage(UIImage(named:"flying"), forState: UIControlState.Normal)
//		buttonEdit.addTarget(self, action: "rightNavItemEditClick:", forControlEvents: UIControlEvents.TouchUpInside)
		buttonEdit.layer.cornerRadius = 0.5 * buttonEdit.bounds.height
		buttonEdit.clipsToBounds = true
		let rightBarButtonItemEdit: UIBarButtonItem = UIBarButtonItem(customView: buttonEdit)
		self.navigationItem.setRightBarButtonItem(rightBarButtonItemEdit, animated: false)
	}
	
	func setLeftBarButtonItem() {
		let buttonLabel: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
		buttonLabel.setTitle(routeObject.name.capitalizedString, forState: UIControlState.Normal)
		buttonLabel.frame = CGRectMake(0, 0, 40, 80)
		let leftLabel: UIBarButtonItem = UIBarButtonItem(customView: buttonLabel)
		self.navigationItem.setLeftBarButtonItem(leftLabel, animated: false)
	}
	
	// LOCATION FUNCTIONS *******************
	
	func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
		child.lblGpsAccuracy.text = "\(manager.location.horizontalAccuracy)"
		if manager.location.horizontalAccuracy < 51 {
			if routeObject.stopCoordsList.isEmpty == true {
				routeObject.addCoords(currentCoords!)
				incrementStopCount()
			}
			
			let moveDistance = manager.location.distanceFromLocation(currentCoords)
			distance = distance + moveDistance
			
			println("move distance = \(moveDistance)")
			currentCoords = manager.location
			println("distance = \(distance)")
			child.lblGpsAccuracy.textColor = UIColor(red: 15/255, green: 181/255, blue: 196/255, alpha: 1)
			child.lblDistance.text = "\(distance) km"
		} else {
			child.lblGpsAccuracy.textColor = UIColor.redColor()
		}
	}
	
	func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
		println("didFailWithError")
		println(error)
	}
	
	func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
		if (status == CLAuthorizationStatus.AuthorizedAlways) || (status == CLAuthorizationStatus.AuthorizedWhenInUse) {
			
		} else {
			global.showAlert("Location Not Allowed", message: "Please enable location services for Panic by going to Settings > Privacy > Location Services.")
		}
	}
	
	override func viewWillDisappear(animated: Bool) {
		if self.isMovingFromParentViewController() == true {
			if tracking == true { end() }
			if !routeObject.coordsList.isEmpty {
				routesHelper.saveToUpload(routeObject)
			}
		}
	}
	
	// SHAPE LAYER ********************
	
	func configure() {
		circle.animateCircle(1.0)
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class CircleView: UIView {
	let circleLayer: CAShapeLayer = CAShapeLayer()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.backgroundColor = UIColor.clearColor()
		let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0), radius: (frame.size.width - 10)/2, startAngle: CGFloat(-M_PI * 0.5), endAngle: CGFloat(M_PI * 1.5), clockwise: true)
		circleLayer.path = circlePath.CGPath
		circleLayer.fillColor = UIColor.clearColor().CGColor
		circleLayer.strokeColor = UIColor.redColor().CGColor
		circleLayer.lineWidth = 5.0;
		circleLayer.strokeEnd = 0.0
		layer.addSublayer(circleLayer)
	}
	
	func animateCircle(duration: NSTimeInterval) {
		let animation = CABasicAnimation(keyPath: "strokeEnd")
		animation.duration = duration
		animation.fromValue = 0
		animation.toValue = 1
		animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
		circleLayer.strokeEnd = 1.0
		circleLayer.addAnimation(animation, forKey: "animateCircle")
	}
	
	func resetCircle() {
		circleLayer.opacity = 0
		circleLayer.removeAllAnimations()
		circleLayer.strokeEnd = 0.0
		circleLayer.opacity = 1
	}

	required init(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}
}
