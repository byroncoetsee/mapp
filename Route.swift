//
//  Route.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/04/10.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit
import CoreLocation

class Route: NSObject {
	
	var id : Int!
	var date : NSDate!
	var name : String!
	var from : String!
	var to : String!
	var mode : String!
	var company : String!
	var start: String! /// Dont use
	var end :String! /// Dont use
	
	var stopCount : Int = 0
	var duration : Int = 0
	var distance : Double = 0.0
	
	var coordsList : [CLLocation] = []
	var stopCoordsList : [CLLocation] = []
	
	init(name : String, from : String, to : String, mode :String, company : String, start : String, end : String) {
		
		self.id = routesHelper.getNewId()
		self.date = NSDate()
		self.name = name
		self.from = from
		self.to = to
		self.mode = mode
		self.company = company
		self.start = start
		self.end = end
	}
	
	func addCoords(coords : CLLocation) {
		if coordsList.isEmpty {
			coordsList = [coords]
			addStopCoords(coords)
		} else {
			coordsList.append(coords)
		}
		routesHelper.updateSavedRoute(self)
	}
	
	func addStopCoords(coords : CLLocation) {
		if stopCoordsList.isEmpty {
			stopCoordsList = [coords]
		} else {
			stopCoordsList.append(coords)
		}
		routesHelper.updateSavedRoute(self)
	}
}
