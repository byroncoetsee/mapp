//
//  NearMeViewController.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/04/16.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class NearMeViewController: UIViewController, MKMapViewDelegate,  CLLocationManagerDelegate {

	@IBOutlet weak var map: MKMapView!
	@IBOutlet weak var btnMyRoutes: UIBarButtonItem!
	@IBOutlet weak var btnOthersRoutes: UIBarButtonItem!
	
	var manager: CLLocationManager! = CLLocationManager()
	var centerMapLocation: dispatch_once_t = 0
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		var theSpan: MKCoordinateSpan = MKCoordinateSpanMake(0.5, 0.5)
		var theRegion: MKCoordinateRegion = MKCoordinateRegionMake(CLLocationCoordinate2DMake(-33.9206605, 18.424724), theSpan)
		map.setRegion(theRegion, animated: true)
		map.showsUserLocation = true
		map.mapType = MKMapType.Standard
		map.showsUserLocation = true
		map.showsPointsOfInterest = true
		map.showsBuildings = true
		map.delegate = self
		
		manager.delegate = self
		manager.desiredAccuracy = kCLLocationAccuracyHundredMeters
		manager.requestAlwaysAuthorization()
		manager.startUpdatingLocation()
		manager.activityType = CLActivityType.AutomotiveNavigation
		
    }
	
	override func viewWillAppear(animated: Bool) {
		self.navigationController?.navigationBarHidden = false
	}
	
	@IBAction func myRoutes(sender: AnyObject) {
		fillMyRoutes()
	}
	
	@IBAction func othersRoutes(sender: AnyObject) {
	}
	
	func refreshRoutes() {
		
	}
	
	func fillMyRoutes() {
		map.removeAnnotations(map.annotations)
		map.removeOverlays(map.overlays)
		
		for (id, route) in routesHelper.uploadRoutes {
			println(route.name)
			println(route.coordsList)
			if !route.coordsList.isEmpty {
				drawPolyline(route)
			}
			
		}
	}
	
	func fillOthersRoutes() {
		
	}
	
	func drawPolyline(route : Route) {
		var points : [CLLocationCoordinate2D] = []
		for point in route.coordsList {
			points.append(point.coordinate)
		}
		var line = MKPolyline(coordinates: &points, count: points.count)
		map.addOverlay(line, level: MKOverlayLevel.AboveRoads)
		
		route.coordsList.first
		
		//		for point in coordsArray {
		var anno = MKPointAnnotation()
		anno.coordinate = CLLocationCoordinate2DMake(route.coordsList.first!.coordinate.latitude, route.coordsList.first!.coordinate.longitude)
		anno.title = route.name
		anno.subtitle = "Start"
		map.addAnnotation(anno)
		anno = MKPointAnnotation()
		anno.coordinate = CLLocationCoordinate2DMake(route.coordsList.last!.coordinate.latitude, route.coordsList.last!.coordinate.longitude)
		anno.title = route.name
		anno.subtitle = "Finish"
		map.addAnnotation(anno)
		//		}
	}

	// ************* MAP FUNCTIONS
	
	func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
		if overlay is MKPolygon {
			var polygonRenderer = MKPolygonRenderer(overlay: overlay)
			polygonRenderer.fillColor = UIColor.cyanColor().colorWithAlphaComponent(0.2)
			polygonRenderer.strokeColor = UIColor.blueColor().colorWithAlphaComponent(0.7)
			polygonRenderer.lineWidth = 3
			return polygonRenderer
		}
		
		if overlay is MKPolyline {
			var polyline = MKPolylineRenderer(overlay: overlay)
			polyline.strokeColor = UIColor.blueColor().colorWithAlphaComponent(0.6)
			polyline.lineWidth = 8
			return polyline
		}
		return nil
	}
	
	// ************** LOCATION FUNCTIONS
	
	func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
	{
		if manager.location.horizontalAccuracy < 60 {
			dispatch_once(&centerMapLocation, {
				if manager.location != nil {
					var theSpan: MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
					var theRegion: MKCoordinateRegion = MKCoordinateRegionMake(manager.location.coordinate, theSpan)
					self.map.setRegion(theRegion, animated: true)
				}
			})
		}
		//        getClosestStop()
	}
	
	override func viewWillDisappear(animated: Bool) {
		manager.stopUpdatingLocation()
		manager = nil
		map = nil
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
