//
//  NewRouteViewController.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/04/08.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit

class NewRouteViewControllerOLD: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
	
	@IBOutlet weak var txtRouteName: UITextField!
	@IBOutlet weak var txtStart: UITextField!
	@IBOutlet weak var txtEnd: UITextField!
	
	@IBOutlet weak var imageModeOfTransport: UIImageView!
	@IBOutlet weak var btnModesOfTransport: UIButton!
	@IBOutlet weak var btnTransportCompany: UIButton!
	@IBOutlet weak var viewPicker: UIVisualEffectView!
	@IBOutlet weak var picker: UIPickerView!
	@IBOutlet weak var lblPickerType: UILabel!
	@IBOutlet weak var btnDone: UIButton!
	
	@IBOutlet weak var btnCancel: UIButton!
	@IBOutlet weak var btnNext: UIButton!
	
	// Variables
	
//	var menuButton : VBFPopFlatButton!
//	var barMenuButton : UIBarButtonItem!
	
	var pickerType : String = ""
	var modeOfTransport : String?
	var transportCompany : String?
	var modes : [String] = ["Train", "Bus", "Taxi", "Boat", "Plane", "Unicycle"]
	var companies : [String] = ["Metrofail", "GA", "SAA"]
	
	// Constraints
	
//	@IBOutlet weak var layoutBtnModeOfTransportLeft: NSLayoutConstraint!
	
	

    override func viewDidLoad() {
        super.viewDidLoad()
		self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"Cancel", style:.Plain, target:nil, action:nil)
		self.navigationItem.setRightBarButtonItem(UIBarButtonItem(title: "Create", style: .Plain, target: nil, action: nil), animated: true)
		self.navigationItem.rightBarButtonItem?.target = self
		self.navigationItem.rightBarButtonItem?.action = "create"
		
		let tapGesture = UITapGestureRecognizer(target: self, action: "hideKeyboard")
		self.view.addGestureRecognizer(tapGesture)

		if imageModeOfTransport.image == nil {
//			layoutBtnModeOfTransportLeft.constant = -imageModeOfTransport.frame.width - 8
			btnModesOfTransport.layoutIfNeeded()
		}
        // Do any additional setup after loading the view.
    }
	
	@IBAction func openModeOfTransport(sender: AnyObject) {
		pickerType = "modeOfTransport"
		lblPickerType.text = "Mode Of Transport"
		viewPicker.hidden = false
	}
	
	@IBAction func openTransportCompany(sender: AnyObject) {
		pickerType = "transportCompany"
		lblPickerType.text = "Transport Company"
		viewPicker.hidden = false
	}
	
	func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		if pickerType == "modeOfTransport" {
			return modes.count
		} else {
			return companies.count
		}
	}
	
	func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
		if pickerType == "modeOfTransport" {
			return modes[row]
		} else {
			return companies[row]
		}
	}
	
	func create() {
		let mode = btnModesOfTransport.titleLabel?.text
		let company = btnTransportCompany.titleLabel?.text
		let route = Route(name: txtRouteName.text, from: txtStart.text, to: txtEnd.text, mode: mode!, company: company!, start: "", end: "")
		routesHelper.saveRoute(route)
		
		let vc = self.storyboard?.instantiateViewControllerWithIdentifier("captureViewController") as! CaptureViewController
		vc.routeObject = route
		self.presentViewController(vc, animated: true, completion: nil)
	}
	
	@IBAction func closePickerView(sender: AnyObject) {
		if pickerType == "modeOfTransport" {
			modeOfTransport = modes[picker.selectedRowInComponent(0)]
			btnModesOfTransport.setTitle(modeOfTransport, forState: UIControlState.Normal)
		} else {
			transportCompany = companies[picker.selectedRowInComponent(0)]
			btnTransportCompany.setTitle(transportCompany, forState: UIControlState.Normal)
		}
		viewPicker.hidden = true
	}
	
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		if textField == txtRouteName {
			txtStart.becomeFirstResponder()
		} else if textField == txtStart {
			txtEnd.becomeFirstResponder()
		} else {
			txtEnd.resignFirstResponder()
		}
		return true
	}
	
	func hideKeyboard() {
		txtRouteName.resignFirstResponder()
		txtStart.resignFirstResponder()
		txtEnd.resignFirstResponder()
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
