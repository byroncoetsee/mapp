//
//  NewRouteTableViewController.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/04/24.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit

class NewRouteTableViewController: UITableViewController {
	
	@IBOutlet weak var txtName: UITextField!
	@IBOutlet weak var txtStart: UITextField!
	@IBOutlet weak var txtEnd: UITextField!
	@IBOutlet weak var btnTransportCompany: UIButton!
	@IBOutlet weak var btnVehicleType: UIButton!
	@IBOutlet weak var txtVehicleCapacity: UITextField!
	@IBOutlet weak var txtFare: UITextField!
	@IBOutlet weak var txtDescription: UITextView!
	
	var parentObject : NewRouteViewController!
	var btnCompanyInitText: String!
	var btnModeInitText: String!

    override func viewDidLoad() {
        super.viewDidLoad()
		
    }
	
	func validation() -> Bool {
		var message = ""
		
		if count(trim(txtName.text)) < 5 { message = "Please fill in the route name\n" }
		if evalStandardString(txtName.text) == false { message = message + "Route name can only contain letters and numbers\n" }
		if count(trim(txtStart.text)) < 5 { message = message + "Start Name must be at least 5 characters\n" }
		if evalStandardString(txtStart.text) == false { message = message + "Start Name can only contain letters and numbers\n" }
		if count(trim(txtEnd.text)) < 5 { message = message + "End Name must be at least 5 characters\n" }
		if evalStandardString(txtEnd.text) == false { message = message + "End Name can only contain letters and numbers\n" }
		
		if parentObject.transportCompany == nil { message = message + "Please choose a transport company\n" }
		if parentObject.modeOfTransport == nil { message = message + "Please choose a transport mode\n" }
		
		if count(message) > 0 {
			global.showAlert("", message: message)
			return false
		}
		return true
	}
	
	func evalStandardString(str : String) -> Bool {
		let regEx = "[A-Za-z0-9.-[:blank:]]+"
		var test = NSPredicate(format: "SELF MATCHES %@", regEx)
		return test.evaluateWithObject(trim(str))
	}
	
	func trim(str : String) -> String {
		return str.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
	}
	
	override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 44
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
