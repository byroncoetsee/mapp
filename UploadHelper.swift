//
//  UploadHelper.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/05/11.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit
import AFNetworking
import CoreLocation

var uploadHelper : UploadHelper = UploadHelper()

class UploadHelper: NSObject, NSURLConnectionDelegate {
	
	var resultData: NSMutableData = NSMutableData(base64Encoding: "")
	
	func upload (route : Route) {

		let protoBuilder = Gometromapper.Upload.Route.builder()
		
		var count = 0
		var points : [Gometromapper.Upload.Route.Point] = []
		loop:
		for point in route.coordsList {
//			points.append(pointBuilder(point))
			count++
			if count == 10 { break loop }
		}
		
		count = 0
		var stops : [Gometromapper.Upload.Route.Stop] = []
		loop:
		for point in route.stopCoordsList {
//			stops.append(stopBuilder(point))
			count++
			if count == 2 { break loop }
		}
		
		protoBuilder.routeName = route.name
		protoBuilder.point = points
		protoBuilder.stop = stops
		protoBuilder.startTime = 100000
		protoBuilder.transportMode = "Mad-Fast-Taxi"
		protoBuilder.transportCompany = "Mad-Fast-Taxis"
		protoBuilder.endPoint = "end point" // route.end
		protoBuilder.farePrice = 10.50
		protoBuilder.currency = "Cash Monies"
		protoBuilder.oneWay = true
		protoBuilder.commonReasonForTrip = "Sommer net"
		protoBuilder.ratingDrivingStyle = 5
		protoBuilder.ratingComfort = 5
		protoBuilder.ratingSaftey = 0
		protoBuilder.routeComments = "I swear this mad fast taxi can fly"
		
		let routeUploadObject = protoBuilder.build()
		
		println(routeUploadObject)
		
		if let dataFromString = "\(routeUploadObject)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
			let json = JSON(data: dataFromString)
			println(json)
		}
		
//		println(json)
		
//		let jsonProto = routeUploadObject.data()
		
//		println(routeUploadObject)
		
		var bytes: [UInt8] = [];
		for code in "\(routeUploadObject)".utf8 {
			bytes.append(UInt8(code));
		}
		println()
		println("SMAL NUMBERs")
//		println(bytes)
		
		var buffer = [UInt8](count:routeUploadObject.data().length, repeatedValue:0)
		routeUploadObject.data().getBytes(&buffer, length: routeUploadObject.data().length)
		println()
		println("BIG NUMBERs")
//		println(buffer)
		
		var chars : String = ""
		for char in buffer {
			chars = "\(chars)\(char)"
		}
//		println()
//		println()
//		println(chars)
		
//		var jsonError: NSError?
//		let jsonData: AnyObject? = NSJSONSerialization.JSONObjectWithData(person.data(), options: NSJSONReadingOptions.AllowFragments, error: &jsonError)
//		if jsonError != nil {
//			println(jsonError)
//		}
//		println(jsonData)
		
		
//		output!.open()
//		output!.write(&buffer, maxLength: person.data().length)
		
//		person.writeToOutputStream(output!)
//		output!.close()
		
//		let bundle = NSBundle.mainBundle().pathForResource("output", ofType: "txt")
//		let content: String = String(contentsOfFile: bundle!, encoding: NSUTF8StringEncoding, error: nil)!
//		println(content)
		
//		let manager = AFHTTPSessionManager(baseURL: NSURL(string: "http://54.68.55.70:9000"))
//		manager.requestSerializer = AFJSONRequestSerializer()
//		manager.responseSerializer = AFJSONResponseSerializer()
//		let baseUrl = NSURL(string: "http://54.68.55.70:9000")
		let baseUrl = NSURL(string: "http://192.168.0.29:9000")
		let manager = AFHTTPRequestOperationManager(baseURL: baseUrl)
		manager.requestSerializer.setValue("application/multipart-form", forHTTPHeaderField: "Content-Type")
		manager.requestSerializer.stringEncoding.bigEndian
		
//		let params = ["data" : routeUploadObject.data()] // IosString
//		let params = ["imei" : "ios123", "data" : routeUploadObject.data()] // RouteTesting
//		let params : NSString = "[\"imei\" : \"ios123\", \"data\" : \"\(buffer)\"]"
//		let jsonDataReady = params.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
//		let jsonData: AnyObject? = NSJSONSerialization.JSONObjectWithData(jsonDataReady!, options: NSJSONReadingOptions.AllowFragments, error: &jsonError)
		
//		if jsonError != nil {
//			println(jsonError)
//		}
//		println(jsonData)
		
//		manager.POST("/uploadIosString", parameters: params, success: {
//		manager.POST("/uploadRouteTesting", parameters: params, success: {
//			(task: NSURLSessionDataTask!, responseObject: AnyObject!) in
//				println("success")
//				let response = task.response as! NSHTTPURLResponse
//				println(response.statusCode)
//				println(response)
//			}, failure: {
//				(task: NSURLSessionDataTask!, error: NSError!) in
//				println("error")
//		})
//		
//		// ========
				
//		"hello".writeToFile("/Users/byroncoetsee/Documents/Programming/GoMetro/Mapp/Mapp/File.txt", atomically: false, encoding: NSUTF8StringEncoding, error: nil)
//		let text2 = String(contentsOfFile: "File.txt", encoding: NSUTF8StringEncoding, error: nil)
//		println(text2)
//		let text2 = String.stringWithContentsOfFile("/Users/byroncoetsee/Documents/Programming/GoMetro/Mapp/Mapp/File")
		
//		manager.POST("/iosNsDataUploadRoute", parameters: params, success: {
//		manager.POST("/iosUploadRoute", parameters: params, success: {
//				( task : AFHTTPRequestOperation!, responseObject : AnyObject!) in
//			println("success")
//				task.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as Set<NSObject>
//				let response = task.response as NSHTTPURLResponse
//				println(response.statusCode)
//				println(response.allHeaderFields)
//				println(response.textEncodingName)
//		}, failure: {
//			(task: AFHTTPRequestOperation!, error: NSError!) in
//				task.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as Set<NSObject>
//				println("error")
//				println(error)
////				println(NSString(data: task.responseData, encoding: NSUTF8StringEncoding))
//		})
		// ===========
		
		
		let postVars = "imei=ios123&data=\(routeUploadObject.data())"
//		let postVars = "data='\(jsonProto)'"
//
		let url = NSURL(string: "http://192.168.0.29:9000/iosUploadRoute")
////		let url = NSURL(string: "http://54.68.55.70:9000/uploadIosString")
//		
//		
//		// =======
		let request = NSMutableURLRequest(URL: url!)
		request.HTTPMethod = "POST"
		request.setValue("ios", forHTTPHeaderField: "user-agent")
////		request.setValue("\(postVars.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))", forHTTPHeaderField: "Content-Length")
//		request.setValue("ascii", forHTTPHeaderField: "charset")
//		request.setValue("application/octet-stream", forHTTPHeaderField: "Content-Type")
//		// ========
//		
//		
////		println(person.data().bytes.encode())
////		println(person.data().bytes.debugDescription)
//		
////		let stream: NSInputStream = NSInputStream(data: person.data())
////		stream.open()
//		
////		println(postVars)
////		request.HTTPBodyStream = stream
////				request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
////				request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
////		println(request.HTTPBody)
//		
//		// ==========
		request.HTTPBody = postVars.dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: false)!
//		println(NSString(data: request.HTTPBody!, encoding: NSASCIIStringEncoding))
//		println(request.allHTTPHeaderFields)
		let urlConnection = NSURLConnection(request: request, delegate: self)
		
	}
	
	func pointBuilder(locationPoint: CLLocationCoordinate2D) -> Gometromapper.Upload.Route.Point {
		var newPoint = Gometromapper.Upload.Route.Point.builder()
		newPoint.lat = Float(locationPoint.latitude)
		newPoint.lon = Float(locationPoint.longitude)
		newPoint.timeoffset = 0
		return newPoint.build()
	}
	
	func stopBuilder(locationPoint: CLLocationCoordinate2D) -> Gometromapper.Upload.Route.Stop {
		var newPoint = Gometromapper.Upload.Route.Stop.builder()
		newPoint.lat = Float(locationPoint.latitude)
		newPoint.lon = Float(locationPoint.longitude)
		return newPoint.build()
	}
	
	//NSURLConnection delegate method
	func connection(connection: NSURLConnection, didFailWithError error: NSError) {
		println("Failed with error:\(error.localizedDescription)")
	}
	
	//NSURLConnection delegate method
	func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
		println("Response")
		println("...............\(response)")
	}
	
	//NSURLConnection delegate method
	func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
		println("DATA")
//		print(data)
		resultData.appendData(data)
	}
	
	//NSURLConnection delegate method
	func connectionDidFinishLoading(connection: NSURLConnection!) {
		println("connectionDidFinishLoading")
		let data = NSString(data: resultData, encoding: NSUTF8StringEncoding)
//		println(data)
	}
}

class Point: NSObject {
	var lat: Double = 0
	var long: Double = 0
	var timeOffset: Int = 0
	
	init (locationPoint: CLLocation) {
		lat = locationPoint.coordinate.latitude
		long = locationPoint.coordinate.longitude
//		timeOffset = locationPoint.timestamp NSDATE
	}
}
