//
//  Global.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/04/09.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit

var global : Global = Global()

class Global: UIViewController {
	
	var persistant = NSUserDefaults.standardUserDefaults()
	var dateFormatter : NSDateFormatter = NSDateFormatter()
	
	func showAlert(title: String, message: String)
	{
		var alert: UIAlertView = UIAlertView()
		alert.addButtonWithTitle("OK")
		alert.title = title
		alert.message = message
		alert.show()
	}
}