//
//  UploadHelper.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/05/11.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit
import AFNetworking
import CoreLocation

var uploadHelperJson : UploadHelperJson = UploadHelperJson()

class UploadHelperJson: NSObject, NSURLConnectionDelegate {
	
	var resultData: NSMutableData = NSMutableData(base64Encoding: "")
	var jsonDict: [String : AnyObject] = [:]
	
	func upload (route : Route) {
		
		jsonBuilder(route)
		let json: JSON = JSON(jsonDict)
		sendJson(json)
		println(json)
	}
	
	func sendJson(json : JSON) {
		let postVars = "imei=ios123&data=\(json.rawString(encoding: NSUTF8StringEncoding, options: NSJSONWritingOptions.allZeros)!)"
		let url = NSURL(string: "http://54.68.55.70:9000/iosUploadRoute")
//		let url = NSURL(string: "http://192.168.0.29:9000/iosUploadRoute")
		let request = NSMutableURLRequest(URL: url!)
		request.HTTPMethod = "POST"
		request.setValue("ios", forHTTPHeaderField: "user-agent")
		request.HTTPBody = postVars.dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: false)!
		let urlConnection = NSURLConnection(request: request, delegate: self)
	}
	
	func jsonBuilder(route : Route) {
		
		addKey("routeName", value: route.name)
		addKey("startPoint", value: route.from)
		addKey("endPoint", value: route.to)
		addKey("startTime", value: "100000")
		addKey("transportCompany", value: route.company)
		addKey("transportMode", value: route.mode)
		addKey("farePrice", value: "10.50")
		addKey("currency", value: "R")
		addKey("oneWay", value: "true")
		addKey("commonReasonForTrip", value: "Sommer Net")
		addKey("ratingDrivingStyle", value: "0")
		addKey("ratingComfort", value: "0")
		addKey("ratingSaftey", value: "0")
		addKey("routeComments", value: "I swear this mad fast taxi can fly")
		
		if !route.coordsList.isEmpty { addPoints("points", pointsList: route.coordsList) }
		if !route.stopCoordsList.isEmpty { addPoints("stops", pointsList: route.stopCoordsList) }
	}
	
	func addKey(key : String, value : String) {
		jsonDict[key] = value
	}
	
	func addPoints(type: String, pointsList : [CLLocation]) {
		var count = 0
		var pointDict : [String : [String : Double]] = [:]
		for point in pointsList {
			pointDict["\(count)"] = ["lat" : point.coordinate.latitude, "long": point.coordinate.longitude, "time" : point.timestamp.timeIntervalSince1970]
			count++
		}
//		let sortedKeysAndValues = sorted(pointDict) { $0.0 < $1.0 }
		jsonDict[type] = pointDict
	}

	//NSURLConnection delegate method
	func connection(connection: NSURLConnection, didFailWithError error: NSError) {
		println("Failed with error:\(error.localizedDescription)")
	}
	
	//NSURLConnection delegate method
	func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
		println("Response")
		if let httpResponse = response as? NSHTTPURLResponse {
			if httpResponse.statusCode == 200 {
				global.showAlert("Success", message: "Route successfully uploaded. Thank you!")
			} else {
				global.showAlert("Unsuccessful", message: "Route upload was unsuccessful. Check you internet connection.")
			}
		}
	}
	
	//NSURLConnection delegate method
	func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
		println("DATA")
		resultData.appendData(data)
	}
	
	//NSURLConnection delegate method
	func connectionDidFinishLoading(connection: NSURLConnection!) {
		println("connectionDidFinishLoading")
		let data = NSString(data: resultData, encoding: NSUTF8StringEncoding)
	}
}
