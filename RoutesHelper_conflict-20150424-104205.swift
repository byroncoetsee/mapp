//
//  RoutesHelper.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/04/10.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit

var routesHelper : RoutesHelper = RoutesHelper()

class RoutesHelper: NSObject {
	
	var savedRoutes : [Int : Route] = [:]
	var uploadRoutes : [Int : Route] = [:]
	
	func getNewId () -> Int {
		
		var number = Int(arc4random_uniform(99999))
		println(number)
		while (find(savedRoutes.keys, number) != nil) {
			println("RAN WHILE LOOP - THIS IS WEIRD")
			number = Int(arc4random_uniform(99999))
		}
		return  number
	}
	
	func saveRoute(route : Route) {
		savedRoutes[route.id] = route
		saveUserDefaults()
	}
	
	func deleteSavedRoute(route : Route) {
		println(savedRoutes)
		savedRoutes.removeValueForKey(route.id)
		saveUserDefaults()
		println(savedRoutes)
	}
	
	func deleteUploadRoute(route : Route) {
//		println(uploadRoutes)
		uploadRoutes.removeValueForKey(route.id)
		saveUserDefaults()
//		println(uploadRoutes)
	}
	
	/// Update a Route objects coords list to UserDefaults
	func updateSavedRoute(route : Route) {
		savedRoutes[route.id] = route
		saveUserDefaults()
	}
	
	func saveToUpload(route : Route) {
		savedRoutes[route.id] = nil
		uploadRoutes[route.id] = route
		println("Route saved to UPLOAD - \(route)")
		saveUserDefaults()
	}
	
	func uploadToSaved(route : Route) {
		savedRoutes[route.id] = route
		uploadRoutes[route.id] = nil
		println("Route saved to SAVED - \(route)")
		saveUserDefaults()
	}
	
	func uploadToServer() {
		uploadHelper.upload()
	}
	
	func saveUserDefaults() {
		var routeArchive : [String : NSData] = [:]
		
		// Archiving data from Saved
		for (id, route) in savedRoutes {
			let data = NSData(data: NSKeyedArchiver.archivedDataWithRootObject(route))
			routeArchive["\(id)"] = data
		}
		global.persistant.setObject(routeArchive, forKey: "savedRoutes")
		
		// Archiving data from Upload
		for (id, route) in uploadRoutes {
			let data = NSData(data: NSKeyedArchiver.archivedDataWithRootObject(route))
			routeArchive["\(id)"] = data
		}
		global.persistant.setObject(routeArchive, forKey: "uploadRoutes")
		global.persistant.synchronize()
	}
	
	func loadUserDefaults() {
		if global.persistant.objectForKey("savedRoutes") != nil {
			let data = global.persistant.objectForKey("savedRoutes") as! [String : NSData]
			for (id, route) in data {
				savedRoutes[id.toInt()!] = NSKeyedUnarchiver.unarchiveObjectWithData(route) as? Route
			}
		}
		
		if global.persistant.objectForKey("uploadRoutes") != nil {
			let data = global.persistant.objectForKey("uploadRoutes") as! [String : NSData]
			for (id, route) in data {
				uploadRoutes[id.toInt()!] = NSKeyedUnarchiver.unarchiveObjectWithData(route) as? Route
			}
		}
	}
}
