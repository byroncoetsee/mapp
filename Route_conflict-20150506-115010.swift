//
//  Route.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/04/10.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit
import CoreLocation

class Route: NSObject, NSCoding {
	
	var id : Int!
	var date : NSDate!
	var name : String!
	var from : String!
	var to : String!
	var mode : String!
	var company : String!
	var start: String!
	var end :String!
	
	var stopCount : Int = 0
	var duration : Int = 0
	var distance : Double = 0.0
	
	var coordsList : [CLLocationCoordinate2D] = []
	var stopCoordsList : [CLLocationCoordinate2D] = []
	
	init(name : String, from : String, to : String, mode :String, company : String, start : String, end : String) {
		
		self.id = routesHelper.getNewId()
		self.date = NSDate()
		self.name = name
		self.from = from
		self.to = to
		self.mode = mode
		self.company = company
		self.start = start
		self.end = end
	}
	
	func addCoords(coords : CLLocation) {
		if coordsList.isEmpty {
			coordsList = [coords.coordinate]
			addStopCoords(coords)
		} else {
			coordsList.append(coords.coordinate)
		}
		routesHelper.updateSavedRoute(self)
	}
	
	func addStopCoords(coords : CLLocation) {
		if stopCoordsList.isEmpty {
			stopCoordsList = [coords.coordinate]
		} else {
			stopCoordsList.append(coords.coordinate)
		}
		routesHelper.updateSavedRoute(self)
	}
	
	func encodeWithCoder(aCoder: NSCoder) {
		aCoder.encodeObject(id, forKey: "id")
		aCoder.encodeObject(date, forKey: "date")
		aCoder.encodeObject(name, forKey: "name")
		aCoder.encodeObject(from, forKey: "from")
		aCoder.encodeObject(to, forKey: "to")
		aCoder.encodeObject(mode, forKey: "mode")
		aCoder.encodeObject(company, forKey: "company")
		aCoder.encodeObject(start, forKey: "start")
		aCoder.encodeObject(end, forKey: "end")
		aCoder.encodeObject(stopCount, forKey: "stopCount")
		aCoder.encodeObject(duration, forKey: "duration")
		aCoder.encodeObject(distance, forKey: "distance")
		
		// Route Coordinate List
		
		var sizeOfCoordsList = coordsList.count * sizeof(CLLocationCoordinate2D)
		aCoder.encodeObject(sizeOfCoordsList, forKey: "sizeOfCoordsList")
//		println(sizeOfCoordsList)
		
		var coordData = NSData(bytes: &coordsList, length: sizeOfCoordsList)
		aCoder.encodeObject(coordData, forKey: "coords")
//		println(coordData)
		
		// Stop Coordinate List
		
		sizeOfCoordsList = stopCoordsList.count * sizeof(CLLocationCoordinate2D)
		aCoder.encodeObject(sizeOfCoordsList, forKey: "sizeOfStopCoordsList")
//		println(sizeOfCoordsList)
		
		coordData = NSData(bytes: &stopCoordsList, length: sizeOfCoordsList)
		aCoder.encodeObject(coordData, forKey: "stopCoords")
//		println(coordData)
	}
	
	required init(coder aDecoder: NSCoder) {
		id = aDecoder.decodeObjectForKey("id") as! Int
		date = aDecoder.decodeObjectForKey("date") as! NSDate
		name = aDecoder.decodeObjectForKey("name") as! String
		from = aDecoder.decodeObjectForKey("from") as! String
		to = aDecoder.decodeObjectForKey("to") as! String
		mode = aDecoder.decodeObjectForKey("mode") as! String
		company = aDecoder.decodeObjectForKey("company") as! String
		stopCount = aDecoder.decodeObjectForKey("stopCount") as! Int
		duration = aDecoder.decodeObjectForKey("duration") as! Int
		distance = aDecoder.decodeObjectForKey("distance") as! Double
		
		// Route Coordinate List
		
		var sizeOfCoordsList = aDecoder.decodeObjectForKey("sizeOfCoordsList") as! Int
		var coordData = aDecoder.decodeObjectForKey("coords") as! NSData
		var sizeOfArray = sizeOfCoordsList/sizeof(CLLocationCoordinate2D)
		coordsList = [CLLocationCoordinate2D](count: sizeOfArray, repeatedValue: CLLocationCoordinate2DMake(0.0, 0.0))
		coordData.getBytes(&coordsList, length: sizeOfCoordsList)
		
		// Stop Coordinate List
		
		sizeOfCoordsList = aDecoder.decodeObjectForKey("sizeOfStopCoordsList") as! Int
		coordData = aDecoder.decodeObjectForKey("stopCoords") as! NSData
		sizeOfArray = sizeOfCoordsList/sizeof(CLLocationCoordinate2D)
		stopCoordsList = [CLLocationCoordinate2D](count: sizeOfArray, repeatedValue: CLLocationCoordinate2DMake(0.0, 0.0))
		coordData.getBytes(&stopCoordsList, length: sizeOfCoordsList)
	}
}
