//
//  ViewController.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/04/07.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit
import VBFPopFlatButton

class ViewController: UIViewController, UIGestureRecognizerDelegate {
	
	// Controls

	@IBOutlet weak var btnMenu: UIButton!
	@IBOutlet weak var viewMenu: UIVisualEffectView!
	@IBOutlet weak var imageProfilePic: UIImageView!
	@IBOutlet weak var imageBackground: UIImageView!
	@IBOutlet weak var btnNew: UIButton!
	@IBOutlet weak var btnSaved: UIButton!
	@IBOutlet weak var btnUpload: UIButton!
	@IBOutlet weak var btnNearMe: UIButton!
	var menuButton : VBFPopFlatButton!
	var barMenuButton : UIBarButtonItem!
	
	// Variables
	
	var animatingMenu = false
	
	// Constraints
	
	@IBOutlet weak var layoutMenuLeft: NSLayoutConstraint!
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		println(btnMenu.frame)
		menuButton = VBFPopFlatButton(frame: btnMenu.frame, buttonType: FlatButtonType.buttonMenuType, buttonStyle: FlatButtonStyle.buttonPlainStyle, animateToInitialState: true)
		menuButton.addTarget(self, action: "toggleMenu", forControlEvents: UIControlEvents.TouchUpInside)
		menuButton.tintColor = UIColor(red:0.07, green:0.62, blue:0.71, alpha:1)
		var barMenuButton = UIBarButtonItem(customView: menuButton)
		self.navigationItem.leftBarButtonItem = barMenuButton
		
		self.navigationController?.navigationBar.barTintColor = UIColor(red:0.18, green:0.48, blue:0.52, alpha:1)
		self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
		self.navigationController!.navigationBar.titleTextAttributes = NSDictionary(object: UIColor.whiteColor(), forKey: NSForegroundColorAttributeName) as [NSObject : AnyObject]
		
		let swipeGesture = UISwipeGestureRecognizer(target: self, action: "toggleMenu")
		swipeGesture.direction = UISwipeGestureRecognizerDirection.Left
		viewMenu.addGestureRecognizer(swipeGesture)
		viewMenu.layer.shadowOffset = CGSizeZero
		viewMenu.layer.shadowOpacity = 0.8
		viewMenu.layer.shadowRadius = 4
		
		roundButton(btnNew, shadow: true)
		roundButton(btnSaved, shadow: true)
		roundButton(btnUpload, shadow: true)
		roundButton(btnNearMe, shadow: true)
		
		imageProfilePic.layer.cornerRadius = 0.5 * imageProfilePic.bounds.size.width
		imageProfilePic.layer.borderColor = UIColor.whiteColor().CGColor
		imageProfilePic.layer.borderWidth = 2
		imageProfilePic.clipsToBounds = true
		
		toggleMenu()
	}
	
	override func viewWillAppear(animated: Bool) {
		self.navigationController?.navigationBarHidden = true
	}
	
	@IBAction func menu(sender: AnyObject) {
		toggleMenu()
	}
	
	func toggleMenu() {
		if animatingMenu == false {
			animatingMenu = true
			
			if layoutMenuLeft.constant == 0 {
				menuButton.animateToType(FlatButtonType.buttonMenuType)
				layoutMenuLeft.constant = -viewMenu.frame.width - 10
			} else {
				layoutMenuLeft.constant = 0
				menuButton.animateToType(FlatButtonType.buttonCloseType)
			}
			
			UIView.animateWithDuration(0.6, animations: {
				self.view.layoutIfNeeded()
				}, completion: {
					(result: Bool) -> Void in
					self.animatingMenu = false
			})
		}
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	}
	
	@IBAction func newRoute(sender: AnyObject) {
	}
	
	@IBAction func savedRoutes(sender: AnyObject) {
	}
	
	@IBAction func uploadRoute(sender: AnyObject) {
	}
	
	@IBAction func nearMeRoutes(sender: AnyObject) {
	}
	
	func roundButton(button: UIButton, borderWidth: CGFloat = 0, borderColour: CGColor = UIColor.grayColor().CGColor, shadow: Bool = false) -> UIButton {
		
		button.backgroundColor = UIColor.whiteColor()
		
		button.tintColor = UIColor(red:0.07, green:0.62, blue:0.71, alpha:1)
		button.layer.cornerRadius = 0.5 * button.bounds.size.width
		
		if shadow == true {
			button.layer.shadowOffset = CGSizeZero
			button.layer.shadowColor = UIColor.blackColor().CGColor
			button.layer.shadowOpacity = 0.6
		}
		
		button.layer.borderColor = borderColour
		button.layer.borderWidth = borderWidth
		
		return button
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		println("MEMORY WARNING")
	}


}

