//
//  CaptureViewController.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/04/09.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit
import CoreLocation

class CaptureViewControllerOLD: UIViewController, CLLocationManagerDelegate {
	
	@IBOutlet weak var imageModeOfTransport: UIImageView!
	@IBOutlet weak var lblTransportCompany: UILabel!
	@IBOutlet weak var lblRouteName: UILabel!
	@IBOutlet weak var lblFrom: UILabel!
	@IBOutlet weak var lblTo: UILabel!
	@IBOutlet weak var lblGpsAccuracy: UILabel!
	@IBOutlet weak var btnIncrementStopCount: UIButton!
	@IBOutlet weak var lblStopCount: UILabel!
	@IBOutlet weak var lblDuration: UILabel!
	@IBOutlet weak var lblDistance: UILabel!
	@IBOutlet weak var txtFare: UITextField!
	@IBOutlet weak var btnStart: UIButton!
	@IBOutlet weak var btnDone: UIButton!
	
	// Variables
	
	var routeObject : Route!
	var manager : CLLocationManager = CLLocationManager()
	var timerRecordCoords : NSTimer?
	var timerDuration : NSTimer?
	var routeCoords : [CLLocation] = []
	var currentCoords : CLLocation?
	var tracking : Bool = false
	
	var stopCount = 0
	var duration = 0
	var distance = 0.0
	

    override func viewDidLoad() {
        super.viewDidLoad()
		manager.requestAlwaysAuthorization()
		manager.delegate = self
		manager.distanceFilter = 10
		manager.desiredAccuracy = kCLLocationAccuracyBest
		
		self.navigationController?.navigationBar.barTintColor = UIColor(red:0.33, green:0.32, blue:0.32, alpha:1)
		self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"Back", style:.Plain, target:nil, action:nil)
		
		for coord in routeObject.coordsList {
			routeCoords.append(CLLocation(latitude: coord.latitude, longitude: coord.longitude))
			btnStart.setTitle("RESTART", forState: UIControlState.Normal)
		}
		
		stopCount = routeObject.stopCount
		duration = routeObject.duration
		distance = routeObject.distance
		
		lblTransportCompany.text = routeObject.company
		lblRouteName.text = routeObject.name
		lblFrom.text = routeObject.from
		lblTo.text = routeObject.to
		
		lblStopCount.text = "\(stopCount)"
		lblDuration.text = "\(duration)"
		lblDistance.text = "\(distance)"
    }
	
	@IBAction func start(sender: AnyObject) {
		
		if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.AuthorizedAlways {
			tracking = !tracking
			if tracking == true {
				if routeCoords.isEmpty {
					start()
				} else {
					var saveAlert = UIAlertController(title: "Are you sure?", message: "This will clear your progress and restart the route", preferredStyle: UIAlertControllerStyle.Alert)
					saveAlert.addAction(UIAlertAction(title: "Yes", style: .Destructive, handler: { (action: UIAlertAction!) in
						self.start()
					}))
					saveAlert.addAction(UIAlertAction(title: "No", style: .Default, handler: { (action: UIAlertAction!) in }))
					presentViewController(saveAlert, animated: true, completion: nil)
				}
			} else {
				end()
			}
		}
	}
	
	@IBAction func incrementStopCount(sender: AnyObject) {
		++stopCount
		lblStopCount.text = "\(stopCount)"
	}
	
	func incrementDuration() {
		++duration
		if duration < 60 {
			lblDuration.text = "\(duration) s"
		} else {
			lblDuration.text = "\(Int(duration/60)) m"
		}
		
	}
	
	func saveCoords() {
		if currentCoords != nil {
			routeObject.addCoords(currentCoords!)
		}
	}
	
	func start() {
		self.navigationController?.navigationBar.barTintColor = UIColor(red:0.89, green:0, blue:0.24, alpha:1)
		btnStart.setTitle("FINISH", forState: UIControlState.Normal)
		self.routeCoords = []
		self.stopCount = 0
		self.duration = 0
		self.distance = 0
		timerRecordCoords = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "saveCoords", userInfo: nil, repeats: true)
		timerDuration = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "incrementDuration", userInfo: nil, repeats: true)
		lblStopCount.text = "\(stopCount)"
		lblDuration.text = "\(duration)"
		lblDistance.text = "\(distance) km"
		manager.startUpdatingLocation()
		lblGpsAccuracy.text = "9999m"
		btnIncrementStopCount.hidden = false
	}
	
	func end() {
		self.navigationController?.navigationBar.barTintColor = UIColor(red:0.33, green:0.32, blue:0.32, alpha:1)
		manager.stopUpdatingLocation()
		btnStart.setTitle("RESTART", forState: UIControlState.Normal)
		timerRecordCoords?.invalidate()
		timerRecordCoords = nil
		timerDuration?.invalidate()
		timerDuration = nil
		currentCoords = nil
		lblGpsAccuracy.text = "OFF"
		btnIncrementStopCount.hidden = true
		
		for coord in routeObject.coordsList {
			println(coord)
		}
	}
	
	// LOCATION FUNCTIONS *******************
	
	func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
		lblGpsAccuracy.text = "\(manager.location.horizontalAccuracy)"
		if manager.location.horizontalAccuracy < 51 {
			let moveDistance = manager.location.distanceFromLocation(currentCoords)
			distance = distance + moveDistance
			
			println("move distance = \(moveDistance)")
			currentCoords = manager.location
			println("distance = \(distance)")
			lblGpsAccuracy.textColor = UIColor(red: 15/255, green: 181/255, blue: 196/255, alpha: 1)
			lblDistance.text = "\(distance) km"
		} else {
			lblGpsAccuracy.textColor = UIColor.redColor()
		}
	}
	
	func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
		println("didFailWithError")
		println(error)
	}
	
	func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
		if (status == CLAuthorizationStatus.AuthorizedAlways) || (status == CLAuthorizationStatus.AuthorizedWhenInUse) {
			
		} else {
			global.showAlert("Location Not Allowed", message: "Please enable location services for Panic by going to Settings > Privacy > Location Services.\nWithout location services, no one will be able to respond to your emergency.")
		}
	}

	@IBAction func done(sender: AnyObject) {
		end()
		if !routeObject.coordsList.isEmpty {
			routesHelper.saveToUpload(routeObject)
		}
		self.dismissViewControllerAnimated(true, completion: nil)
	}
	
	override func viewWillDisappear(animated: Bool) {
		self.navigationController?.navigationBar.barTintColor = UIColor(red:0.18, green:0.48, blue:0.52, alpha:1)
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
