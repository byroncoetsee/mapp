//
//  SavedRoutesTableViewCell.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/04/13.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit

class SavedRoutesTableViewCell: UITableViewCell {
	
	@IBOutlet weak var lblName: UILabel!
	@IBOutlet weak var lblFromTo: UILabel!
	
	var from : String!
	var to : String!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
	
	func build() {
		lblFromTo.text = "\(from)  - \(to)"
	}

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
