//
//  RoutesHelper.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/04/10.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit
import CoreLocation

var routesHelper : RoutesHelper = RoutesHelper()

class RoutesHelper: NSObject {
	
	var savedRoutes : [Int : Route] = [:]
	var uploadRoutes : [Int : Route] = [:]
	var jsonDict: [String : AnyObject] = [:]
	var jsonDictFinal: [String : AnyObject] = [:]
	var jsonCount : Int = 0
	
	func getNewId () -> Int {
		
		var number = Int(arc4random_uniform(99999))
		println(number)
		while (find(savedRoutes.keys, number) != nil) {
			println("RAN WHILE LOOP - THIS IS WEIRD")
			number = Int(arc4random_uniform(99999))
		}
		return  number
	}
	
	func saveRoute(route : Route) {
		savedRoutes[route.id] = route
		saveUserDefaults()
	}
	
	func deleteSavedRoute(route : Route) {
		println(savedRoutes)
		savedRoutes.removeValueForKey(route.id)
		saveUserDefaults()
		println(savedRoutes)
	}
	
	func deleteUploadRoute(route : Route) {
		uploadRoutes.removeValueForKey(route.id)
		saveUserDefaults()
	}
	
	/// Update a Route objects coords list to UserDefaults
	func updateSavedRoute(route : Route) {
		savedRoutes[route.id] = route
		saveUserDefaults()
	}
	
	func updateUploadRoute(route : Route) {
		println(uploadRoutes[route.id]?.from)
		uploadRoutes[route.id] = route
		println(uploadRoutes[route.id]?.from)
		saveUserDefaults()
	}
	
	func saveToUpload(route : Route) {
		savedRoutes[route.id] = nil
		uploadRoutes[route.id] = route
		println("Route saved to UPLOAD - \(route)")
		saveUserDefaults()
	}
	
	func uploadToSaved(route : Route) {
		savedRoutes[route.id] = route
		uploadRoutes[route.id] = nil
		println("Route saved to SAVED - \(route)")
		saveUserDefaults()
	}
	
	func uploadToServer(route : Route) {
		uploadHelperJson.upload(route)
	}
	
	func saveUserDefaults() {
		
		// Archiving data from Saved
		for (id, route) in savedRoutes {
			jsonBuilder(route)
			endJsonBuild("\(id)")
		}
		saveTofile("saved", json: JSON(jsonDictFinal))
		
		// Archiving data from Upload
		for (id, route) in uploadRoutes {
			jsonBuilder(route)
			endJsonBuild("\(id)")
		}
		saveTofile("upload", json: JSON(jsonDictFinal))
	}
	
	func saveTofile(type : String, json : JSON) {
		let file = "\(type).txt"
		let dirs: [String]? = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) as? [String]

		if (dirs != nil) {
			let directories:[String] = dirs!
			let dirs = directories[0]; //documents directory
			let path = dirs.stringByAppendingPathComponent(file);
			let text = json.rawData(options: NSJSONWritingOptions.allZeros, error: nil)
//			let text = json.rawString(encoding: NSUTF8StringEncoding, options: nil)!

			//writing
			text?.writeToFile(path, atomically: false)
//			text.writeToFile(path, atomically: false, encoding: NSUTF8StringEncoding, error: nil);
		}
		jsonDict = [:]
		jsonDictFinal = [:]
	}
	
	func loadUserDefaults() {
		readFromFile("saved")
		readFromFile("upload")
	}
	
	func readFromFile(type : String) {
		var file : String = "\(type).txt"
		
		let dirs: [String]? = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) as? [String]
		
		if (dirs != nil) {
			let directories:[String] = dirs!
			let dirs = directories[0]; //documents directory
			let path = dirs.stringByAppendingPathComponent(file);
			var error:NSError?
			let text = String(contentsOfFile: path, encoding:NSUTF8StringEncoding, error: &error)
			
			if let data = NSData(contentsOfFile: path) {
				let json = JSON(data: data, options: NSJSONReadingOptions.AllowFragments, error: nil)
				processFileText(json, type: type)
		   }
		}
	}
	
	func processFileText(json : JSON, type : String) {
		var dict : [Int : Route] = [:]
		
		for (id, record) in json {
			println(record)
			
			let name = record["routeName"].string
			let from = record["startPoint"].string
			let to = record["endPoint"].string
			let mode = record["transportMode"].string
			let company = record["transportCompany"].string
			
			var route = Route(name: name!, from: from!, to: to!, mode: mode!, company: company!, start: "", end: "")
			route.id = id.toInt()
			
			if let points = record["points"].array {
				for point in points {
					let latLong = CLLocationCoordinate2DMake(point["lat"].double! , point["long"].double!)
					let timeStamp = NSDate(timeIntervalSince1970: point["time"].double!)
					let coord = CLLocation(coordinate: latLong, altitude: 0, horizontalAccuracy: 5, verticalAccuracy: 5, timestamp: timeStamp)
					route.addCoords(coord)
				}
			}
			dict[id.toInt()!] = route
		}
		if type == "saved" {
			savedRoutes = dict
		} else {
			uploadRoutes = dict
		}
	}
	
	// JSON
	
	func jsonBuilder(route : Route) {
		
		addKey("routeName", value: route.name)
		addKey("startPoint", value: route.from)
		addKey("endPoint", value: route.to)
		addKey("startTime", value: "100000")
		addKey("transportCompany", value: route.company)
		addKey("transportMode", value: route.mode)
		addKey("farePrice", value: "10.50")
		addKey("currency", value: "Cash Monies")
		addKey("oneWay", value: "true")
		addKey("commonReasonForTrip", value: "Sommer Net")
		addKey("ratingDrivingStyle", value: "5")
		addKey("ratingComfort", value: "5")
		addKey("ratingComfort", value: "5")
		addKey("ratingSaftey", value: "0")
		addKey("routeComments", value: "I swear this mad fast taxi can fly")
		
		if !route.coordsList.isEmpty { addPoints("points", pointsList: route.coordsList) }
		if !route.stopCoordsList.isEmpty { addPoints("stops", pointsList: route.stopCoordsList) }
	}
	
	func addKey(key : String, value : String) {
		jsonDict[key] = value
	}
	
	func addPoints(type: String, pointsList : [CLLocation]) {
		var count = 0
		var pointDict : [String : [String : Double]] = [:]
		for point in pointsList {
			pointDict["\(count)"] = ["lat" : point.coordinate.latitude, "long": point.coordinate.longitude, "time" : point.timestamp.timeIntervalSince1970]
			count++
		}
		//		let sortedKeysAndValues = sorted(pointDict) { $0.0 < $1.0 }
		jsonDict[type] = pointDict
	}
	
	func endJsonBuild(id : String) {
		jsonDictFinal[id] = jsonDict
		jsonDict = [:]
		jsonCount++
	}
}
