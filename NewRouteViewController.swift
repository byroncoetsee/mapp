//
//  NewRouteViewController.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/04/24.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit

class NewRouteViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
	
	@IBOutlet weak var container : UIView!
	@IBOutlet weak var pickerView: UIPickerView!
	@IBOutlet weak var viewPicker: UIVisualEffectView!
	
	var modes : [String] = ["Train", "Bus", "Taxi", "Boat", "Plane", "Unicycle"]
	var companies : [String] = ["Metrofail", "GA", "SAA"]
	
	var info : NewRouteTableViewController!
	var pickerType : String = ""
	var modeOfTransport : String?
	var transportCompany : String?

	override func viewDidLoad() {
        super.viewDidLoad()
		
		self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"Cancel", style:.Plain, target:nil, action:nil)
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Create", style: .Plain, target: self, action: "create")
		
		info = self.childViewControllers[0] as! NewRouteTableViewController
		info.parentObject = self
		info.btnTransportCompany.addTarget(self, action: "openTransportCompany", forControlEvents: .TouchUpInside)
		info.btnVehicleType.addTarget(self, action: "openModeOfTransport", forControlEvents: .TouchUpInside)
    }
	
	override func viewWillAppear(animated: Bool) {
		self.navigationController?.navigationBarHidden = false
	}
	
	// ********** Actions
	
	@IBAction func closeViewPicker(sender: AnyObject) {
		if pickerType == "modeOfTransport" {
			modeOfTransport = modes[pickerView.selectedRowInComponent(0)]
			info.btnVehicleType.setTitle(modeOfTransport, forState: .Normal)
		} else {
			transportCompany = companies[pickerView.selectedRowInComponent(0)]
			info.btnTransportCompany.setTitle(transportCompany, forState: .Normal)
		}
		fadeViewPicker(fadeIn: false)
	}
	
	// ********* Functions
	
	func openModeOfTransport() {
		pickerType = "modeOfTransport"
		fadeViewPicker()
	}
	
	func openTransportCompany() {
		pickerType = "transportCompany"
		fadeViewPicker()
	}
	
	func fadeViewPicker(fadeIn : Bool = true) {
		info.tableView.endEditing(true)
		UIView.animateWithDuration(0.2, animations: {
			if fadeIn == true {
				self.pickerView.reloadAllComponents()
				self.viewPicker.hidden = false
				self.viewPicker.alpha = 0.95
			} else {
				self.viewPicker.alpha = 0.0
			}
			}, completion: {
				(result : Bool) -> Void in
				if fadeIn == false {
					self.viewPicker.hidden = true
				}
		})
	}
	
	func create() {
		if info.validation() == false {
			let mode = info.btnVehicleType.titleLabel?.text
			let company = info.btnTransportCompany.titleLabel?.text
			
			let route = Route(name: info.txtName.text, from: info.txtStart.text, to: info.txtEnd.text, mode: mode!, company: company!, start: "", end: "")
			routesHelper.saveRoute(route)
			
			let vc = self.storyboard?.instantiateViewControllerWithIdentifier("captureViewController") as! CaptureViewController
			vc.routeObject = route
			vc.navigationController?.navigationItem.leftBarButtonItem?.title = "Done"
			self.navigationController?.pushViewController(vc, animated: true)
		}
	}
	
	// ******** Picker stuff
	
	func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		if pickerType == "modeOfTransport" {
			return modes.count
		} else {
			return companies.count
		}
	}
	
	func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
		var myString = ""
		if pickerType == "modeOfTransport" {
			myString = modes[row]
		} else {
			myString = companies[row]
		}
		return NSAttributedString(string: myString, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
	}


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
