//
//  CaptureTableViewController.swift
//  Mapp
//
//  Created by Byron Coetsee on 2015/05/05.
//  Copyright (c) 2015 GoMetro Pty (Ltd). All rights reserved.
//

import UIKit

class CaptureTableViewController: UITableViewController {
	
	@IBOutlet weak var imageModeOfTransport: UIImageView!
//	@IBOutlet weak var lblTransportCompany: UILabel!
	@IBOutlet weak var txtFrom: UITextField!
	@IBOutlet weak var txtTo: UITextField!
	@IBOutlet weak var lblGpsAccuracy: UILabel!
//	@IBOutlet weak var btnIncrementStopCount: UIButton!
//	@IBOutlet weak var lblStopCount: UILabel!
	@IBOutlet weak var lblDuration: UILabel!
	@IBOutlet weak var lblDistance: UILabel!
	@IBOutlet weak var txtFare: UITextField!
//	@IBOutlet weak var btnStart: UIButton!
//	@IBOutlet weak var btnDone: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
		
    }
	
	override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 44
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
